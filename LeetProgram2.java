import java.io.*;
class Demo{

	public static void main (String [] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter haystack : ");
		String str1= br.readLine();

		System.out.print("Enter needle : ");
		String str2= br.readLine();

		int firstIndex = str1.indexOf(str2);

		System.out.println(firstIndex);
	}
}
