import java.io.*;
class Demo{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter no. of rows : ");
		int row = Integer.parseInt(br.readLine());

		char ch= 'A';

		for (int i=0; i< row; i++){

			char ch2= ch;
		
			for(int j=1; j<= row; j++){
			
				System.out.print(ch2 + " ");
				ch2++;
			}

			ch++;
		
			System.out.println();

		}
	
	
	}
}
